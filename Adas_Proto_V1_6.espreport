{
  "inputs":[
    "Adas_Proto_V1_6.mdxml"
  ],
  "stats":{
    "executionStart":"2024-07-01T12:35:58.549319036Z",
    "duration_ms":120,
    "info":0,
    "warning":7,
    "error":0,
    "ruleCount":42,
    "ruleExecutionCount":500
  },
  "rules":[
    {
      "id":"RULE_UML_ACT_C_819",
      "label":"Fork Node Configuration - Output Edges",
      "description":"Fork Node must have at least one output edge.",
      "severity":"ERROR",
      "classes":[
        "Node"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_818",
      "label":"Fork Node Configuration - Input Edge",
      "description":"Fork Node must have strictly one input edge.",
      "severity":"ERROR",
      "classes":[
        "Node"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_89",
      "label":"Configuration of Object Flow Ends",
      "description":"Object Flow ends should reference a pin or a node with a parameter. \nObject Flow ends must have the same type or at least one end should reference a type. \nFix: Define only one end with a type or set the same type for both ends.",
      "severity":"ERROR",
      "classes":[
        "Edge"
      ]
    },
    {
      "id":"RULE_UML_OB_C_81",
      "label":"Incorrect Language",
      "description":"For an OpaqueBehavior or an OpaqueExpression language should be set to \"C\" when using C code generator.",
      "severity":"WARNING",
      "classes":[
        "XmiObject"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_83",
      "label":"Unsupported Structural Nodes",
      "description":"Structueral nodes are not supported. These nodes are identified as following: \n-\tSerquence Node \n-\tConditionnal Node \n-\tLoop Node \nFix: Do not use these nodes in your activity.",
      "severity":"ERROR",
      "classes":[
        "XmiObject"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_811",
      "label":"No Output Control/Object Flow for an Activity Final Node",
      "description":"An Activity Final Node shall not have an output control/object flow. \nFix: Delete the control/object flow.",
      "severity":"ERROR",
      "classes":[
        "Node"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_84",
      "label":"Unsupported ExceptionHandler and RaiseExceptionAction",
      "description":"Exception Handler is not supported. \nFix: Do not use it in your activity.",
      "severity":"ERROR",
      "classes":[
        "XmiObject"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_810",
      "label":"Activity Parameter Node Connection",
      "description":"An ActivityParameter Node can only be connected to one single Object Flow. \nFix: Only connect one Object Flow to the Activity Parameter Node.",
      "severity":"ERROR",
      "classes":[
        "Node"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_81",
      "label":"Edge crossing structured activities border",
      "description":"In the context of a structured activity node, it is forbidden to have an edge crossing its border. \nRationale: An edge crossing a structured activity node border can be seen as a switch between activities when realized. Such operation is similar to a context switch which is not supported.\nFix: Do not use such pattern in you activity.",
      "severity":"ERROR",
      "classes":[
        "Edge"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_813",
      "label":"Create Object Action Configuration - Classifier",
      "description":"A Create Object Action must reference a classifier.",
      "severity":"ERROR",
      "classes":[
        "Node"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_82",
      "label":"Unsupported Variable Actions",
      "description":"Actions in relation with the local variables are not supported. Actions are known as: \n-\tClear Variable Action, \n-\tRead Variable Action, \n-\tAdd Variable Value Action, \n-\tRemove Variable Value Action. \nFix: Do not use these actions in your activity.",
      "severity":"ERROR",
      "classes":[
        "XmiObject"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_812",
      "label":"Create Object Action Configuration - Output Pin",
      "description":"A Create Object Action can only have 1 output Pin.",
      "severity":"ERROR",
      "classes":[
        "Node"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_87",
      "label":"Unsupported Interruptible Activity Region",
      "description":"Interruptible Activity Region is not supported. \nFix: Do not use it in your activity.",
      "severity":"ERROR",
      "classes":[
        "XmiObject"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_815",
      "label":"Decision Node Configuration - Output Edge",
      "description":"Decision Node must have at least one output edge.",
      "severity":"ERROR",
      "classes":[
        "Node"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_88",
      "label":"Unsupported Accept Events without Input Edge",
      "description":"Accept Event without input edge is not supported. \nAccept Events without input edge are always active, typical pattern is a permanent execution of a timer. \nFix: Do not use such pattern in your activity.",
      "severity":"ERROR",
      "classes":[
        "Node"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_814",
      "label":"Create Object Action Configuration - Referenced Type",
      "description":"Output pin of Create Object Action shall reference the same classifier or shall not reference any.",
      "severity":"ERROR",
      "classes":[
        "Node"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_85",
      "label":"Unsupported Cardinality of Pins",
      "description":"A cardinality different of 1 for input pin and output pin is not supported. \nFix: Use only a cardinality of 1 for input pins and output pins.",
      "severity":"ERROR",
      "classes":[
        "XmiObject"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_817",
      "label":"Decision Node Configuration - Opaque Behavior",
      "description":"Guard of edge must be an opaqueBehavior.",
      "severity":"ERROR",
      "classes":[
        "Node"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_86",
      "label":"Unsupported Expansion Region",
      "description":"Expansion Region is not supported.\nFix: Do not use it in your activity.",
      "severity":"ERROR",
      "classes":[
        "XmiObject"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_816",
      "label":"Decision Node Configuration - Else Guard",
      "description":"Decision Node must have at most one \"else\" guard. \nNote: Empty guard is considered as an \"else\" guard.",
      "severity":"ERROR",
      "classes":[
        "Node"
      ]
    },
    {
      "id":"ESP_COMM_001",
      "label":"Name Required",
      "description":"All object must have a name.",
      "severity":"ERROR",
      "classes":[
        "ESPEntity"
      ]
    },
    {
      "id":"ESP_COMM_003",
      "label":"Execution Requires a Referenced Data",
      "description":"Data must be set for a DataReceivedExecution or a DataSendCompletedExecution.",
      "severity":"ERROR",
      "classes":[
        "DataReceivedExecution",
        "DataSendCompletedExecution"
      ]
    },
    {
      "id":"ESP_COMM_002",
      "label":"Execution Requires a Referenced Port",
      "description":"Port must be set for a DataReceivedExecution, DataSendCompletedExecution or OperationInvokedExecution.",
      "severity":"ERROR",
      "classes":[
        "DataReceivedExecution",
        "DataSendCompletedExecution",
        "OperationInvokedExecution"
      ]
    },
    {
      "id":"ESP_COMM_005",
      "label":"Execution Periodic Shall Be Consistent",
      "description":"A valid period must be set for a PeriodicExecution.",
      "severity":"ERROR",
      "classes":[
        "PeriodicExecution"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_820",
      "label":"Fork Node Configuration - Referenced Type",
      "description":"Fork Node has either at least one edge having a type or all edges having a type must have the same type.",
      "severity":"ERROR",
      "classes":[
        "Node"
      ]
    },
    {
      "id":"ESP_COMM_004",
      "label":"ComponentInstance Requires a Referenced Type",
      "description":"ComponentTnstance must reference an existing Component or Assembly.",
      "severity":"ERROR",
      "classes":[
        "ComponentInstance"
      ]
    },
    {
      "id":"RULE_UML_813",
      "label":"Trigger Improvement Configuration",
      "description":"Description\tImprovements proposal:\nFor triggers in a statemachine or in an activity, it is good to specify the source port to optimize the generated code.\nRationale: without any specified port, the generated code has to read each port to detect if an event is raised.",
      "severity":"WARNING",
      "classes":[
        "XmiObject"
      ]
    },
    {
      "id":"RULE_C_71",
      "label":"RULE_C_71.label",
      "description":"RULE_C_71.description",
      "severity":"ERROR",
      "classes":[
        "Data",
        "Type",
        "EnumLitteral",
        "Field"
      ]
    },
    {
      "id":"RULE_UML_812",
      "label":"Missing Signal",
      "description":"No Signal has been set to SignalEvent",
      "severity":"ERROR",
      "classes":[
        "SignalEvent"
      ]
    },
    {
      "id":"RULE_UML_811",
      "label":"Malformed HTML body ",
      "description":"Check if the parser could read the html inside OpaqueBehavior and OpaqueExpression.",
      "severity":"ERROR",
      "classes":[
        "XmiObject"
      ]
    },
    {
      "id":"INTERNAL_ERROR",
      "label":"INTERNAL_ERROR.label",
      "description":"INTERNAL_ERROR.description",
      "severity":"ERROR",
      "classes":[
      ]
    },
    {
      "id":"ESP_COMM_007",
      "label":"Port requires a referenced Interface",
      "description":"Port must reference an existing Interface.",
      "severity":"ERROR",
      "classes":[
        "Port"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_822",
      "label":"Accept Event Action Configuration - Event Reference  ",
      "description":"AcceptEventAction must reference an Event.",
      "severity":"ERROR",
      "classes":[
        "Node"
      ]
    },
    {
      "id":"ESP_COMM_006",
      "label":"Execution Requires a Referenced Operation",
      "description":"Operation must be set for a OperationInvokedExecution.",
      "severity":"ERROR",
      "classes":[
        "OperationInvokedExecution"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_821",
      "label":"Send Signal Action Configuration ",
      "description":"Send Signal Action must reference a Signal.",
      "severity":"ERROR",
      "classes":[
        "Node"
      ]
    },
    {
      "id":"ESP_COMM_009",
      "label":"ConnectorEnd requires a referenced Port",
      "description":"ConnectorEnd must reference an existing Port.",
      "severity":"ERROR",
      "classes":[
        "ConnectorEnd"
      ]
    },
    {
      "id":"RULE_UML_ACT_C_823",
      "label":"Unsupported Event for Accept Event Action  ",
      "description":"Any Receive Event, Call Event, Change Event declared in an Accept Event Action are not supported. \nOnly SignalEvent and TimeEvent are supported.",
      "severity":"ERROR",
      "classes":[
        "Node"
      ]
    },
    {
      "id":"ESP_COMM_010",
      "label":"Reference Shall Be Consistent",
      "description":"All references must be resolved.",
      "severity":"ERROR",
      "classes":[
        "Model"
      ]
    },
    {
      "id":"ESP_COMM_012",
      "label":"Requires a Type",
      "description":"Some objects require a Type.",
      "severity":"ERROR",
      "classes":[
        "HasType"
      ]
    },
    {
      "id":"ESP_COMM_011",
      "label":"Connected Port Consistency",
      "description":"Connected Ports must reference compatible Interfaces. \nCompatible Interfaces must have compatible Data and compatible Operations. \nCompatible Data have the same name and reference the same Type. \nCompatible Operations have the same Arguments and the same return Type.",
      "severity":"ERROR",
      "classes":[
        "Connector"
      ]
    },
    {
      "id":"ESP_COMM_013",
      "label":"Duplicate Connectors",
      "description":"2 ports cannot have more than one connector in common.",
      "severity":"ERROR",
      "classes":[
        "Assembly"
      ]
    },
    {
      "id":"RULE_UML_822",
      "label":"Recursion in submachine",
      "description":"Recursion in submachine is not supported in this code generator.",
      "severity":"ERROR",
      "classes":[
        "State"
      ]
    }
  ],
  "diagnostics":[
    {
      "timestamp":"2024-07-01T12:35:58.631888585Z",
      "rule":"RULE_UML_OB_C_81",
      "severity":"warning",
      "message":"Language should be set to \"C\" but it is \"English\".",
      "source":{
        "id":"_2024x_2c1010c_1716454043118_58663_41953",
        "type":"OpaqueExpression"
      }
    },
    {
      "timestamp":"2024-07-01T12:35:58.636252313Z",
      "rule":"RULE_UML_OB_C_81",
      "severity":"warning",
      "message":"Language should be set to \"C\" but it is \"English\".",
      "source":{
        "id":"_2024x_2c1010c_1716454043118_144921_41954",
        "type":"OpaqueExpression"
      }
    },
    {
      "timestamp":"2024-07-01T12:35:58.638260397Z",
      "rule":"RULE_UML_OB_C_81",
      "severity":"warning",
      "message":"Language should be set to \"C\" but it is \"English\".",
      "source":{
        "id":"_2024x_2c1010c_1716454043117_215767_41948",
        "type":"OpaqueExpression"
      }
    },
    {
      "timestamp":"2024-07-01T12:35:58.638815883Z",
      "rule":"RULE_UML_OB_C_81",
      "severity":"warning",
      "message":"Language should be set to \"C\" but it is \"English\".",
      "source":{
        "id":"_2024x_2c1010c_1716454043118_370234_41951",
        "type":"OpaqueExpression"
      }
    },
    {
      "timestamp":"2024-07-01T12:35:58.639851814Z",
      "rule":"RULE_UML_OB_C_81",
      "severity":"warning",
      "message":"Language should be set to \"C\" but it is \"English\".",
      "source":{
        "id":"_2024x_2c1010c_1716454043118_752052_41949",
        "type":"OpaqueExpression"
      }
    },
    {
      "timestamp":"2024-07-01T12:35:58.640373891Z",
      "rule":"RULE_UML_OB_C_81",
      "severity":"warning",
      "message":"Language should be set to \"C\" but it is \"English\".",
      "source":{
        "id":"_2024x_2c1010c_1716454043118_297865_41952",
        "type":"OpaqueExpression"
      }
    },
    {
      "timestamp":"2024-07-01T12:35:58.640888668Z",
      "rule":"RULE_UML_OB_C_81",
      "severity":"warning",
      "message":"Language should be set to \"C\" but it is \"English\".",
      "source":{
        "id":"_2024x_2c1010c_1716454043118_816244_41950",
        "type":"OpaqueExpression"
      }
    }
  ]
}