/* @date 2024-07-17T08:53:20.447605864Z */

/**********************************************************************
 * @file Cse_AuthorizeLaneChange_hook.h
 *
 * Automatically generated by Software Producer Engineer
 * Runtime code generator @version 1.0.0
 **********************************************************************/


#ifndef CSE_AUTHORIZELANECHANGE_HOOK_H
#define CSE_AUTHORIZELANECHANGE_HOOK_H

#include "Cse_AuthorizeLaneChange_hook_cfg.h"

#if defined(Cse_ReadHook_AuthorizeLaneChange_p1_data)
#undef ReadHook_AuthorizeLaneChange_p1_data
extern void Cse_ReadHook_AuthorizeLaneChange_p1_data_Start(Boolean* from_UNCONNECTED_p1_data);
extern void Cse_ReadHook_AuthorizeLaneChange_p1_data_Return(Boolean* from_UNCONNECTED_p1_data);
#else
#define Cse_ReadHook_AuthorizeLaneChange_p1_data_Start(value)  ((void) (0))
#define Cse_ReadHook_AuthorizeLaneChange_p1_data_Return(value)  ((void) (0))
#endif /* ReadHook_AuthorizeLaneChange_p1_data */


#if defined(Cse_WriteHook_AuthorizeLaneChange_p1_data)
#undef WriteHook_AuthorizeLaneChange_p1_data
extern void Cse_WriteHook_AuthorizeLaneChange_p1_data_Start(Boolean* from_UNCONNECTED_p1_data);
extern void Cse_WriteHook_AuthorizeLaneChange_p1_data_Return(Boolean* from_UNCONNECTED_p1_data);
#else
#define Cse_WriteHook_AuthorizeLaneChange_p1_data_Start(value)  ((void) (0))
#define Cse_WriteHook_AuthorizeLaneChange_p1_data_Return(value)  ((void) (0))
#endif /* WriteHook_AuthorizeLaneChange_p1_data */

#endif /* CSE_HOOK_H */

/* end of file */
