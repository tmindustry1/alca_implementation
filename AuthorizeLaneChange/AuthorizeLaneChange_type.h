/* @date 2024-07-17T08:53:20.519693417Z */

/**********************************************************************
 * @file AuthorizeLaneChange_type.h
 *
 * Automatically generated by Software Producer Engineer
 * Runtime code generator @version 
 * @generated_from _2024x_2c1010c_1716453837421_264888_38667
 **********************************************************************/

#ifndef AUTHORIZELANECHANGE_TYPE_H
#define AUTHORIZELANECHANGE_TYPE_H

#include <stddef.h>
/* stdbool.h define standard bool (true/false) starting from C99 standard */ 
#include <stdbool.h>

/**********************************************************************
 * Data Type Boolean definition
 **********************************************************************/
#ifndef Boolean_DEFINITION
#define Boolean_DEFINITION
typedef bool Boolean;
#endif /* Boolean */

#endif /* AUTHORIZELANECHANGE_TYPE_H */

/* end of file */
