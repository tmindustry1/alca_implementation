/*2024-07-17T08:53:20.497533760Z*/

/**********************************************************************
 * @file AuthorizeLaneChange_AuthorizeLaneChange.c
 *
 * Automatically generated by Software Production Engineer
 * Statemachine Code Generator @version v1.0.0
 * @generated_from _2024x_2c1010c_1716454017795_152532_41819
 **********************************************************************/

/* Application code for statemachine "AuthorizeLaneChange" */

#include "AuthorizeLaneChange_AuthorizeLaneChange.h"
#include "AuthorizeLaneChange.h"

#define STATEMACHINE        AuthorizeLaneChange_struct

/**********************************************************************
 * Function AuthorizeLaneChange_struct_dtor
 * Destructor generated automatically to clean data structure
 *
 * @generated_from _2024x_2c1010c_1716454017795_152532_41819
 **********************************************************************/
static void AuthorizeLaneChange_struct_dtor(struct AuthorizeLaneChange_struct * const self) {
    /* Clean pointer to destructor */
    self->dtor = NULL;
}
/**********************************************************************
 * Function AuthorizeLaneChange_struct_ctor
 * Constructor generated automatically to initialize data structure
 *
 * @generated_from _2024x_2c1010c_1716454017795_152532_41819
 **********************************************************************/
void AuthorizeLaneChange_struct_ctor(struct AuthorizeLaneChange_struct * const self, struct AuthorizeLaneChange* classifier) \
    {
    /* Init pointer to destructor */
    self->dtor = AuthorizeLaneChange_struct_dtor;

    /* Init Classifier variable */
    self->classifier = classifier;
}
/**********************************************************************
 * separate code of user-modifiable functions to prevent accidental
 * changes to code above.
 **********************************************************************/
#include "AuthorizeLaneChange_AuthorizeLaneChange_usr.c"

/**********************************************************************
 * Function _basic_AuthorizeLaneChange_init
 * Implements statemachine init code
 * Must be called before main loop function
 * Generated automatically. Do not edit content.
 *
 * @generated_from _2024x_2c1010c_1716454017795_152532_41819
 **********************************************************************/
void _basic_AuthorizeLaneChange_init(AuthorizeLaneChange_struct *instance) {
    instance->FIRST = TRUE;
    instance->WALL_CLOCK = (WALLCLOCK_T)0;
    instance->CURRENT_STATE_0 = AUTHORIZELANECHANGE_STATE_T_VOID;
    instance->CURRENT_STATE_1 = AUTHORIZELANECHANGE_STATE_T_VOID;
    instance->CURRENT_STATE_2 = AUTHORIZELANECHANGE_STATE_T_VOID;

    StateMachine_init(instance);
}

/**********************************************************************
 * Function _basic_AuthorizeLaneChange
 * Implements statemachine main loop
 * Must be called after init function
 * Generated automatically. Do not edit content.
 *
 * @generated_from _2024x_2c1010c_1716454017795_152532_41819
 **********************************************************************/
void _basic_AuthorizeLaneChange(AuthorizeLaneChange_struct *instance) {
    if(instance->FIRST != FALSE) {
        instance->FIRST = FALSE;
        /* Transition INITIAL_0 -> Lane Follower Mode */
        /* @generated_from _2024x_2c1010c_1716454043116_837798_41925 */
        instance->CURRENT_STATE_0 = AUTHORIZELANECHANGE_STATE_T_Lane_Follower_Mode;
        /* Transition INITIAL_2 -> Max Speed Limit */
        /* @generated_from _2024x_2c1010c_1716454043117_902466_41936 */
        instance->CURRENT_STATE_2 = AUTHORIZELANECHANGE_STATE_T_Max_Speed_Limit;
    }
    switch(instance->CURRENT_STATE_0) {
        /* State Overtaking Mode */
        /* @generated_from _2024x_2c1010c_1716454043112_955155_41905 */
        case AUTHORIZELANECHANGE_STATE_T_Overtaking_Mode: {
            switch(instance->CURRENT_STATE_1) {
                /* State Right Overtaking */
                /* @generated_from _2024x_2c1010c_1716454043114_486534_41908 */
                case AUTHORIZELANECHANGE_STATE_T_Right_Overtaking: {
                    instance->DONE_ACTIVITY = TRUE;
                    Right_Overtaking_Do(instance); /* Do of AUTHORIZELANECHANGE_STATE_T_Right_Overtaking */
                    if( Overtaking_end_flag ) {
                        /* Transition Right Overtaking -> FINAL_1 */
                        /* @generated_from _2024x_2c1010c_1716454043116_565402_41931 */
                        Right_Overtaking_Exit(instance); /* Exit of AUTHORIZELANECHANGE_STATE_T_Right_Overtaking */
                        Right_Overtaking_Effect(instance); /* Effect of AUTHORIZELANECHANGE_STATE_T_Right_Overtaking */
                        instance->CURRENT_STATE_1 = AUTHORIZELANECHANGE_STATE_T_FINAL_1;
                    } else {
                        /* stay in th state */;
                    }
                    break; /* end of AUTHORIZELANECHANGE_STATE_T_Right_Overtaking */
                }
                /* State Left Overtaking */
                /* @generated_from _2024x_2c1010c_1716454043115_773735_41912 */
                case AUTHORIZELANECHANGE_STATE_T_Left_Overtaking: {
                    instance->DONE_ACTIVITY = TRUE;
                    Left_Overtaking_Do(instance); /* Do of AUTHORIZELANECHANGE_STATE_T_Left_Overtaking */
                    if( Overtaking_end_flag ) {
                        /* Transition Left Overtaking -> FINAL_1 */
                        /* @generated_from _2024x_2c1010c_1716454043116_642575_41930 */
                        Left_Overtaking_Exit(instance); /* Exit of AUTHORIZELANECHANGE_STATE_T_Left_Overtaking */
                        Left_Overtaking_Effect(instance); /* Effect of AUTHORIZELANECHANGE_STATE_T_Left_Overtaking */
                        instance->CURRENT_STATE_1 = AUTHORIZELANECHANGE_STATE_T_FINAL_1;
                    } else {
                        /* stay in th state */;
                    }
                    break; /* end of AUTHORIZELANECHANGE_STATE_T_Left_Overtaking */
                }
                /* State Initialization */
                /* @generated_from _2024x_2c1010c_1716454043115_874416_41916 */
                case AUTHORIZELANECHANGE_STATE_T_Initialization: {
                    if( lane != 0 &&
                    Left_vehicle_presence == false ) {
                        /* Transition Initialization -> Left Overtaking */
                        /* @generated_from _2024x_2c1010c_1716454043116_114201_41929 */
                        Left_Overtaking_Entry(instance); /* Entry of AUTHORIZELANECHANGE_STATE_T_Left_Overtaking */
                        instance->CURRENT_STATE_1 = AUTHORIZELANECHANGE_STATE_T_Left_Overtaking;
                    } else if( lane != 2 &&
                    Right_vehicle_presence == false ) {
                        /* Transition Initialization -> Right Overtaking */
                        /* @generated_from _2024x_2c1010c_1716454043116_478736_41928 */
                        Right_Overtaking_Entry(instance); /* Entry of AUTHORIZELANECHANGE_STATE_T_Right_Overtaking */
                        instance->CURRENT_STATE_1 = AUTHORIZELANECHANGE_STATE_T_Right_Overtaking;
                    } else {
                        /* stay in th state */;
                    }
                    break; /* end of AUTHORIZELANECHANGE_STATE_T_Initialization */
                }
                /* State FINAL_1 */
                /* @generated_from _2024x_2c1010c_1716454043115_104091_41918 */
                case AUTHORIZELANECHANGE_STATE_T_FINAL_1: {
                    break; /* end of AUTHORIZELANECHANGE_STATE_T_FINAL_1 */
                }
                default: break;
            } /* end of switch */
            if( (instance->CURRENT_STATE_1 == AUTHORIZELANECHANGE_STATE_T_FINAL_1) || (instance->CURRENT_STATE_1 == AUTHORIZELANECHANGE_STATE_T_VOID) \
                ) {
                /* Transition Overtaking Mode -> Lane Follower Mode */
                /* @generated_from _2024x_2c1010c_1716454043116_8860_41926 */
                instance->CURRENT_STATE_1 = AUTHORIZELANECHANGE_STATE_T_VOID;
                instance->CURRENT_STATE_0 = AUTHORIZELANECHANGE_STATE_T_Lane_Follower_Mode;
                /* Transition INITIAL_2 -> Max Speed Limit */
                /* @generated_from _2024x_2c1010c_1716454043117_902466_41936 */
                instance->CURRENT_STATE_2 = AUTHORIZELANECHANGE_STATE_T_Max_Speed_Limit;
            } else {
                /* stay in th state */;
            }
            break; /* end of AUTHORIZELANECHANGE_STATE_T_Overtaking_Mode */
        }
        /* State Lane Follower Mode */
        /* @generated_from _2024x_2c1010c_1716454043114_876831_41906 */
        case AUTHORIZELANECHANGE_STATE_T_Lane_Follower_Mode: {
            switch(instance->CURRENT_STATE_2) {
                /* State Control Speed Limit */
                /* @generated_from _2024x_2c1010c_1716454043115_213160_41919 */
                case AUTHORIZELANECHANGE_STATE_T_Control_Speed_Limit: {
                    instance->DONE_ACTIVITY = TRUE;
                    Control_Speed_Limit_Do(instance); /* Do of AUTHORIZELANECHANGE_STATE_T_Control_Speed_Limit */
                    if( Front_vehicle_presence == false ) {
                        /* Transition Control Speed Limit -> Max Speed Limit */
                        /* @generated_from _2024x_2c1010c_1716454043116_112089_41933 */
                        instance->CURRENT_STATE_2 = AUTHORIZELANECHANGE_STATE_T_Max_Speed_Limit;
                    } else if( Left_vehicle_presence == false || Right_vehicle_presence == false ) {
                        /* Transition Control Speed Limit -> FINAL_2 */
                        /* @generated_from _2024x_2c1010c_1716454043116_694043_41934 */
                        instance->CURRENT_STATE_2 = AUTHORIZELANECHANGE_STATE_T_FINAL_2;
                    } else {
                        /* stay in th state */;
                    }
                    break; /* end of AUTHORIZELANECHANGE_STATE_T_Control_Speed_Limit */
                }
                /* State FINAL_2 */
                /* @generated_from _2024x_2c1010c_1716454043116_70187_41922 */
                case AUTHORIZELANECHANGE_STATE_T_FINAL_2: {
                    break; /* end of AUTHORIZELANECHANGE_STATE_T_FINAL_2 */
                }
                /* State Max Speed Limit */
                /* @generated_from _2024x_2c1010c_1716454043116_783634_41923 */
                case AUTHORIZELANECHANGE_STATE_T_Max_Speed_Limit: {
                    instance->DONE_ACTIVITY = TRUE;
                    Max_Speed_Limit_Do(instance); /* Do of AUTHORIZELANECHANGE_STATE_T_Max_Speed_Limit */
                    if( Front_vehicle_presence == true ) {
                        /* Transition Max Speed Limit -> Control Speed Limit */
                        /* @generated_from _2024x_2c1010c_1716454043117_878062_41935 */
                        instance->CURRENT_STATE_2 = AUTHORIZELANECHANGE_STATE_T_Control_Speed_Limit;
                    } else {
                        /* stay in th state */;
                    }
                    break; /* end of AUTHORIZELANECHANGE_STATE_T_Max_Speed_Limit */
                }
                default: break;
            } /* end of switch */
            if( (instance->CURRENT_STATE_2 == AUTHORIZELANECHANGE_STATE_T_FINAL_2) || (instance->CURRENT_STATE_2 == AUTHORIZELANECHANGE_STATE_T_VOID) \
                ) {
                /* Transition Lane Follower Mode -> Overtaking Mode */
                /* @generated_from _2024x_2c1010c_1716454043116_676517_41927 */
                instance->CURRENT_STATE_2 = AUTHORIZELANECHANGE_STATE_T_VOID;
                instance->CURRENT_STATE_0 = AUTHORIZELANECHANGE_STATE_T_Overtaking_Mode;
                /* Transition INITIAL_1 -> Initialization */
                /* @generated_from _2024x_2c1010c_1716454043116_812936_41932 */
                instance->CURRENT_STATE_1 = AUTHORIZELANECHANGE_STATE_T_Initialization;
            } else {
                /* stay in th state */;
            }
            break; /* end of AUTHORIZELANECHANGE_STATE_T_Lane_Follower_Mode */
        }
        default: break;
    } /* end of switch */
}

/**********************************************************************
 * Function _basic_AuthorizeLaneChange_end
 * Implements statemachine exit code
 * Generated automatically. Do not edit content.
 *
 * @generated_from _2024x_2c1010c_1716454017795_152532_41819
 **********************************************************************/
void _basic_AuthorizeLaneChange_end(AuthorizeLaneChange_struct *instance) {
    StateMachine_end(instance);
}

/* end of file */
