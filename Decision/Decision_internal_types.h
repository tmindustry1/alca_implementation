/* @date 2024-06-26T14:34:39.415880917Z */

/************************************************
 * @file Decision_internal_types.h
 *
 * Automatically generated by Software Production Engineer
 * UML Classes Code Generator @version v100
 ***********************************************/

#ifndef DECISION_INTERNAL_TYPES_H
#define DECISION_INTERNAL_TYPES_H

#include <stdbool.h>


/**********************************************************************
 * Data Types used in statemachines and activities definition
 **********************************************************************/

#ifndef Boolean_DEFINITION
#define Boolean_DEFINITION
typedef bool    Boolean;
#endif

#ifndef Integer_DEFINITION
#define Integer_DEFINITION
typedef int     Integer;
#endif

#ifndef Real_DEFINITION
#define Real_DEFINITION
typedef float   Real;
#endif

#ifndef String_DEFINITION
#define String_DEFINITION
typedef char *  String;
#endif

#ifndef TRUE
#define TRUE    true
#endif

#ifndef FALSE
#define FALSE   false
#endif

#endif //#ifndef DECISION_INTERNAL_TYPES_H

/* end of file */
